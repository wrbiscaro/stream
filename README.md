### Solução Proposta

A solução proposta foi criar uma aplicação de console leve e funcional, que contenha: 

- Uma classe principal para fazer a entrada e saída de dados.
- Uma interface Stream com os métodos a serem implementados.
- Uma classe que implemente a interface Stream.

### Arquivos

- Main:
Classe responsável por fazer a leitura dos caracteres, instanciar um objeto da classe Controller e enviar os caracteres para o mesmo. Utiliza os métodos de controle desse objeto e encontra o primeiro caractere que não se repete, exibindo-o ou exibindo um mensagem de erro amigável.

- Stream
Interface com os métodos de controle a serem implementados.

- Controller
Classe responsável por implementar a interface Controller e que contém os métodos que controlam a fila de caractere, retornando os caracteres sempre na ordem em que foram recebidos, caso ainda exista algum na fila.

### Bibliotecas/API/Tecnologias utilizadas

- Linguagem Java

### Testes

Foi gerado um arquivo de texto com diversas combinações de caracteres possíveis e seus respectivos resultados.

### Observações Finais

- Projeto criado na IDE Netbeans.