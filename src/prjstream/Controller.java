/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjstream;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Wallace Biscaro
 */

//Classe responsavel por controlar a stream utilizando fila
public class Controller implements Stream {
    private List<Character> characters;

    public Controller(String input) {
        characters = new LinkedList<>();
        
        for (char c : input.toCharArray()) 
            characters.add(c);
    }

    @Override
    public char getNext() {
        char next = characters.get(0);     
        characters.remove(0);
        
        return next;
    }

    @Override
    public boolean hasNext() {
        return !characters.isEmpty();
    }
}
