/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjstream;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/**
 *
 * @author Wallace Biscaro
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String caracteres = sc.nextLine();
        
        Controller input = new Controller(caracteres);
        
        char character = firstChar(input);
        
        if(character != 0)
            System.out.println(character);
        else
            System.out.println("Não há caractere na stream que não se repete!");
    }
  
    //Metodo responsavel por retornar o primeiro caractere que nao se repete
    public static char firstChar(Stream input) {
        Map<Character, Integer> characters = new LinkedHashMap<>();
        char character = 0;

        while(input.hasNext()) {
            char next = input.getNext();
            
            if(characters.containsKey(next)) { //Se o caractere ja esta na fila, adiciona um ao contador de repeticoes
                int repeat = characters.get(next);
                characters.put(next, ++repeat);
            }
            else //Senao, adiciona o caractere ao fim da fila
                characters.put(next, 1);
        }

        //Percorre a fila de caracteres e se encontar algum sem repeticao, retorna ele
        for(Entry<Character, Integer> c : characters.entrySet())
            if(c.getValue() == 1)
                return c.getKey();
        
        //Chegou ao fim do metodo sem encontrar caractere nao repetido, retorna zero
        return character;
    }
}
