/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjstream;

/**
 *
 * @author Wallace Biscaro
 */
public interface Stream {
    public char getNext();
    public boolean hasNext();
}
